/**
 * Created by markelba on 5/12/16.
 */

export default class Character {
    constructor() {
        
    }
    
    defaults() {
        return {
            intelligence: 0,
            critHitChance: 0,
            critHitDamage: 0,
            improvedAttackSpeedParagon: 0
        };
    }
}