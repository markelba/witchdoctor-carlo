/**
 * Created by markelba on 5/12/16.
 */

export default class Gear {
    constructor() {

    }

    defaults() {
        return {
            useMaskOfJeram: false,
            maskOfJeramPercent: 100,

            bonusDamageCold: 0,
            bonusDamagePhysical: 0,
            bonusDamageFire: 0,
            bonusDamagePoison: 0,

            bonusDamageFetishArmy: 0,
            bonusDamageZombieDogs: 0,
            bonusDamageGargantuan: 0,

            bonusDamageElites: 0,
            bonusDamagePoisonDarts: 0
        }
    }
}
