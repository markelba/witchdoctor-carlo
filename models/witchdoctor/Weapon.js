/**
 * Created by markelba on 5/12/16.
 */

export default class Weapon {
    constructor() {
        
    }
    
    defaults() {
        return {
            weaponDamage: 0.0,
            weaponBaseAttacksPerSecond: 0.0,
            improvedAttackSpeedWeapon: 0
        }
    }
}