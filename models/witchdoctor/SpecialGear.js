/**
 * Created by markelba on 5/12/16.
 */

export default class SpecialGear {
    constructor() {

    }

    defaults() {
        return {
            useTallMansFinger: false,
            tallMansFingerZombieDogCount: 0,

            useTaskerAndTheo: 0,
            bonusAttackSpeedTaskerAndTheo: 0,

            useHarringtonWaistguard: false,

            useOculus: false,
            bonusDamageOculus: 0,

            useFocusAndRestraint: false,
            focusAndRestraintBonus: 0,

            useZuniSixPiece: false
        }
    }
}