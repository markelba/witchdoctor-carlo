/**
 * Created by markelba on 5/12/16.
 */

export default class Gems {
    constructor() {

    }

    defaults() {
        return {
            bonusDamageSimplicitysStrength: 0,
            bonusDamageEnforcer: 0,
            bonusDamageBaneOfTheTrapped: 0,
            bonusDamageBaneOfThePowerful: 0,
            bonusDamagePainEnhancer: 0,
            bonusDamageGogokOfSwiftness: 0,
            bonusDamageZeiStoneOfVengeance: 0,
            bonusDamageEffaciousToxin: 0
        }

    }
}