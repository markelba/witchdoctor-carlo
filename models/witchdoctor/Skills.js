/**
 * Created by markelba on 5/12/16.
 */

export default class Skills {
    constructor() {

    }

    defaults() {
        return {
            selectedFetishType: "",
            selectedZombieDogType: "",
            selectedGargantuanType: "",

            useBigBadVoodoo: false,
            totalBigBadVoodooStackAOE: 0,
            totalBigBadVoodooStackPetAOE: 0,

            usePtvPassive: 0,
            numberOfDogsSacrificedForProvoke: 0,
            useMcParanoia: false,
            usePoisonSpirit: false,
            useJinx: false,
            useChilledToTheBone: false,
            usePirhanas: false,
            strongArmKnockback: 0,

            bonusMultiPlayerAttackSpeed: 0,
            bonusMultiPlayerPetAttackSpeed: 0
        }
    }
}

Skills.prototype.fetishTypeOptions = [
    {"Head Hunter", "Poison"},
    {"Ambush", "Cold"},
    {"Dagger", "Physical"},
    {"Tiki", "Fire"}
];

Skills.prototype.zombieDogOptions = [
    {"Physical", "Physical"},
    {"Rabid", "Poison"},
    {"Burning", "Fire"},
    {"Chilled", "Cold"}
];

Skills.prototype.gargantuanOptions = [
    {"Humangoid", "Cold"},
    {"Restless Giant", "Physical"},
    {"Wrathful Protector", "Fire"},
    {"Big Stinker", "Poison"},
    {"Bruiser", "Fire"},
    {"None", "Physical"}
];
