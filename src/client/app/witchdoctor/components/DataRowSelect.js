import React from 'react';
import {DropdownButton} from 'react-bootstrap';
import {MenuItem} from 'react-bootstrap';

export default class DataRowSelect extends React.Component {
    static makeOptions(options) {
        return options.map((option, value) => {
            return (<MenuItem key={option.value} value={option.value}
                              selected={option.value === value ? "selected" : ""}>
                {option.label}
            </MenuItem>);
        });
    }

    render() {
        let labelClass = this.props.extraExtraPropTitle ? "col-sm-2" : "col-sm-6";
        let colClass = this.props.extraPropTitle ? "col-sm-2" : "col-sm-6";
        let colClassData = this.props.extraPropTitle ? "col-sm-2" : "col-sm-6";

        return (<div className={this.props.rowClassName}>
            <div className="form-group">
                <label htmlFor={this.props.inputId} className={labelClass}>{this.props.title}</label>
                <div className={colClassData}>
                    <DropdownButton id={this.props.inputId} title="Select..." onChange={this.props.onChange}>
                        {DataRowSelect.makeOptions(this.props.options)}
                    </DropdownButton>
                </div>
            </div>
            {this.props.extraPropTitle &&
            (<div className="form-group">
                <label className={colClass}>{this.props.extraPropTitle}</label>
                <div className={colClass}>{this.props.extraPropValue}</div>
            </div>)
            }
            {this.props.extraExtraPropTitle &&
            (<div className="form-group">
                <label className={colClass}>{this.props.extraExtraPropTitle}</label>
                <div className={colClass}>{this.props.extraExtraPropValue}</div>
            </div>)
            }
        </div>);
    }
}