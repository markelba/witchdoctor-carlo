import React from 'react';

export default class DataRowBoolean extends React.Component {
    render() {
        let labelClass = this.props.extraExtraPropTitle ? "col-sm-2" : "col-sm-6";
        let colClass = this.props.extraPropTitle ? "col-sm-2" : "col-sm-6";
        let colClassData = this.props.extraPropTitle ? "col-sm-2" : "col-sm-6";

        return (<div className={this.props.rowClassName}>
            <div className="form-group">
                <label htmlFor={this.props.inputId} className={labelClass}>{this.props.title}</label>
                <div className={colClassData}>
                    <input id={this.props.inputId} type="checkbox" className="form-control" checked={this.props.checked}
                      onChange={this.props.onChange}/>
                </div>
            </div>
            {this.props.extraPropTitle &&
            (<div className="form-group">
                <label className={colClass}>{this.props.extraPropTitle}</label>
                <div className={colClass}>{this.props.extraPropValue}</div>
            </div>)
            }
            {this.props.extraExtraPropTitle &&
            (<div className="form-group">
                <label className={colClass}>{this.props.extraExtraPropTitle}</label>
                <div className={colClass}>{this.props.extraExtraPropValue}</div>
             </div>)
            }
        </div>);
    }
}