import Property from './props/Property';
import CappedProperty from './props/CappedProperty';
import PercentProperty from './props/PercentProperty';
import CappedPercentProperty from './props/CappedPercentProperty';
import TrueFalseProperty from './props/TrueFalseProperty';
import ConstrainedProperty from './props/ConstrainedProperty';

export default class PlayerStatModel {
    constructor() {
        this.intelligenceVal = new Property(-1);
        this.critHitChanceVal = new Property(-1);
        this.critHitDamageVal = new Property(-1);
        this.weaponDamageVal = new Property(-1);
        this.weaponBaseAttacksPerSecondVal = new Property(-1.0);
        this.improvedAttackSpeedWeaponVal = new PercentProperty(-1);
        this.improvedAttackSpeedGearAndParagonVal = new PercentProperty(-1);

        this.fetishDamageGidbinVal = new PercentProperty(-1);
        this.fetishDamageSycophantVal = new PercentProperty(-1);
        this.fetishArmyDartDamageVal = new PercentProperty(-1);
        this.fetishArmyMeleeDamageVal = new PercentProperty(-1);
        this.fetishTikiDamageVal = new PercentProperty(-1);
        this.fetishHeadHunterDamageVal = new PercentProperty(-1);

        this.useMaskOfJeramVal = new TrueFalseProperty(false);
        this.mojPetIncreaseVal = new PercentProperty(-1);
        this.coldDamagePercentVal = new PercentProperty(-1);
        this.physicalDamagePercentVal = new PercentProperty(-1);
        this.fireDamagePercentVal = new PercentProperty(-1);
        this.poisonDamagePercentVal = new PercentProperty(-1);
        this.fetishArmyDamagePercentVal = new PercentProperty(-1);
        this.zombieDogsDamagePercentVal = new PercentProperty(-1);
        this.gargantuanArmyDamagePercentVal = new PercentProperty(-1);
        this.elitesDamagePercentVal = new PercentProperty(-1);
        this.poisonDartsDamagePercentVal = new PercentProperty(-1);

        this.simplicityStrengthPercentVal = new PercentProperty(-1);
        this.enforcerGemPercentVal = new PercentProperty(-1);
        this.baneOfTheTrappedPercentVal = new PercentProperty(-1);
        this.baneOfThePowerfulPercentVal = new PercentProperty(-1);
        this.painEnhancerMobsVal = new CappedProperty(-1, 4);
        this.gogokOfSwiftnessStacksVal = new CappedProperty(-1, 15);
        this.zeisStoneOfVengeancePercentVal = new PercentProperty(-1);
        this.useGemOfEffaciousToxinVal = new TrueFalseProperty(false);

        this.useTallMansFingerVal = new TrueFalseProperty(false);
        this.numberOfDogsForTMFVal = new CappedProperty(-1, 6);
        this.useTaskerAndTheoVal = new TrueFalseProperty(false);
        this.percentIncreasePetAttackSpeedVal = new PercentProperty(-1);
        this.useHarringtonWaistguardVal = new TrueFalseProperty(false);
        this.percentHarringtonDamageBuffVal = new PercentProperty(-1);
        this.useOculusVal = new TrueFalseProperty(false);
        this.percentOculusDamageBuffVal = new PercentProperty(-1);
        this.focusAndRestraintVal = new ConstrainedProperty('no', PlayerStatModel.focusAndRestraintOptions);
        this.useZuniSixPieceVal = new TrueFalseProperty(false);

        this.fetishArmyTypeVal = new ConstrainedProperty(-1, PlayerStatModel.fetishArmyRuneOptions);
        this.zombieDogsTypeVal = new ConstrainedProperty(-1, PlayerStatModel.zombieDogsRuneOptions);
        this.gargantuanTypeVal = new ConstrainedProperty(-1, PlayerStatModel.gargantuanRuneOptions);

        this.useBBVSlamDanceVal = new TrueFalseProperty(false);
        this.totalBBVStackCharacterVal = new CappedProperty(-1, 6);
        this.totalBBVStackPetsVal = new CappedProperty(-1, 6);
        this.multiplayerIncreasedAttackSpeedBuffVal = new PercentProperty(-1);
        this.multiplayerDamageBuffVal = new PercentProperty(-1);
        this.multiplayerAttacksPerSecondBuffVal = new PercentProperty(-1);
        this.multiplayerPetAttacksPerSecondBuffVal = new PercentProperty(-1);
        this.usePierceTheVeilPassiveVal = new TrueFalseProperty(false);
        this.sacrificeProvokeThePackDogsVal = new CappedProperty(-1,4);
        this.useMcParanoiaVal = new TrueFalseProperty(false);
        this.useHauntedVal = new TrueFalseProperty(false);
        this.useChilledToTheBoneVal = new TrueFalseProperty(false);
        this.useHexJinxVal = new TrueFalseProperty(false);
        this.usePiranhasVal = new TrueFalseProperty(false);
        this.percentStrongarmKnockbackVal = new PercentProperty(-1);

        this.useMidnightFeastPassiveVal = new TrueFalseProperty(false);
        this.numberOfStacksSoulHarvestVal = new CappedProperty(-1, 4);
        this.numberOfStacksGFIntelBuffVal = new CappedProperty(-1, 4);
    }


    get intelligence() {
        return this.intelligenceVal.value;
    }

    get critHitChance() {
        return this.critHitChanceVal.value;
    }

    get critHitDamage() {
        return this.critHitDamageVal.value;
    }

    get weaponDamage() {
        return this.weaponDamageVal.value;
    }

    get weaponBaseAttacksPerSecond() {
        return this.weaponBaseAttacksPerSecondVal.value;
    }

    get improvedAttackSpeedWeapon() {
        return this.improvedAttackSpeedWeaponVal.value;
    }

    get improvedAttackSpeedGearAndParagon() {
        return this.improvedAttackSpeedGearAndParagonVal.value;
    }

    get fetishDamageGidbin() {
        return this.fetishDamageGidbinVal.value;
    }

    get fetishDamageSycophant() {
        return this.fetishDamageSycophantVal.value;
    }

    get fetishArmyDartDamage() {
        return this.fetishArmyDartDamageVal.value;
    }

    get fetishArmyMeleeDamage() {
        return this.fetishArmyMeleeDamageVal.value;
    }

    get fetishTikiDamage() {
        return this.fetishTikiDamageVal.value;
    }

    get fetishHeadHunterDamage() {
        return this.fetishHeadHunterDamageVal.value;
    }

    get useMaskOfJeram() {
        return this.useMaskOfJeramVal.value;
    }

    get mojPetIncrease() {
        return this.mojPetIncreaseVal.value;
    }

    get coldDamagePercent() {
        return this.coldDamagePercentVal.value;
    }

    get physicalDamagePercent() {
        return this.physicalDamagePercentVal.value;
    }

    get fireDamagePercent() {
        return this.fireDamagePercentVal.value;
    }

    get poisonDamagePercent() {
        return this.poisonDamagePercentVal.value;
    }

    get fetishArmyDamagePercent() {
        return this.fetishArmyDamagePercentVal.value;
    }

    get zombieDogsDamagePercent() {
        return this.zombieDogsDamagePercentVal.value;
    }

    get gargantuanArmyDamagePercent() {
        return this.gargantuanArmyDamagePercentVal.value;
    }

    get elitesDamagePercent() {
        return this.elitesDamagePercentVal.value;
    }

    get poisonDartsDamagePercent() {
        return this.poisonDartsDamagePercentVal.value;
    }

    get simplicityStrengthPercent() {
        return this.simplicityStrengthPercentVal.value;
    }

    get enforcerGemPercent() {
        return this.enforcerGemPercentVal.value;
    }

    get baneOfTheTrappedPercent() {
        return this.baneOfTheTrappedPercentVal.value;
    }

    get baneOfThePowerfulPercent() {
        return this.baneOfThePowerfulPercentVal.value;
    }

    get painEnhancerMobs() {
        return this.painEnhancerMobsVal.value;
    }

    get gogokOfSwiftnessStacks() {
        return this.gogokOfSwiftnessStacksVal.value;
    }

    get zeisStoneOfVengeancePercent() {
        return this.zeisStoneOfVengeancePercentVal.value;
    }

    get useGemOfEffaciousToxin() {
        return this.useGemOfEffaciousToxinVal.value;
    }

    get useTallMansFinger() {
        return this.useTallMansFingerVal.value;
    }

    get numberOfDogsForTMF() {
        return this.numberOfDogsForTMFVal.value;
    }

    get useTaskerAndTheo() {
        return this.useTaskerAndTheoVal.value;
    }

    get percentIncreasePetAttackSpeed() {
        return this.percentIncreasePetAttackSpeedVal.value;
    }

    get useHarringtonWaistguard() {
        return this.useHarringtonWaistguardVal.value;
    }

    get percentHarringtonDamageBuff() {
        return this.percentHarringtonDamageBuffVal.value;
    }

    get useOculus() {
        return this.useOculusVal.value;
    }

    get percentOculusDamageBuff() {
        return this.percentOculusDamageBuffVal.value;
    }

    get focusAndRestraint() {
        return this.focusAndRestraintVal.value;
    }

    get useZuniSixPiece() {
        return this.useZuniSixPieceVal.value;
    }

    get fetishArmyType() {
        return this.fetishArmyTypeVal.value;
    }

    get zombieDogsType() {
        return this.zombieDogsTypeVal.value;
    }

    get gargantuanType() {
        return this.gargantuanTypeVal.value;
    }

    get useBBVSlamDance() {
        return this.useBBVSlamDanceVal.value;
    }

    get totalBBVStackCharacter() {
        return this.totalBBVStackCharacterVal.value;
    }

    get totalBBVStackPets() {
        return this.totalBBVStackPetsVal.value;
    }

    get multiplayerIncreasedAttackSpeedBuff() {
        return this.multiplayerIncreasedAttackSpeedBuffVal.value;
    }

    get multiplayerDamageBuff() {
        return this.multiplayerDamageBuffVal.value;
    }

    get multiplayerAttacksPerSecondBuff() {
        return this.multiplayerAttacksPerSecondBuffVal.value;
    }

    get multiplayerPetAttacksPerSecondBuff() {
        return this.multiplayerPetAttacksPerSecondBuffVal.value;
    }

    get usePierceTheVeilPassive() {
        return this.usePierceTheVeilPassiveVal.value;
    }

    get sacrificeProvokeThePackDogs() {
        return this.sacrificeProvokeThePackDogsVal.value;
    }

    get useMcParanoia() {
        return this.useMcParanoiaVal.value;
    }

    get useHaunted() {
        return this.useHauntedVal.value;
    }

    get useChilledToTheBone() {
        return this.useChilledToTheBoneVal.value;
    }

    get useHexJinx() {
        return this.useHexJinxVal.value;
    }

    get usePiranhas() {
        return this.usePiranhasVal.value;
    }

    get percentStrongarmKnockback() {
        return this.percentStrongarmKnockbackVal.value;
    }

    get useMidnightFeastPassive() {
        return this.useMidnightFeastPassiveVal.value;
    }

    get numberOfStacksSoulHarvest() {
        return this.numberOfStacksSoulHarvestVal.value;
    }

    get numberOfStacksGFIntelBuff() {
        return this.numberOfStacksGFIntelBuffVal.value;
    }


    set intelligence(val) {
        this.intelligenceVal.value = val;
    }

    set critHitChance(val) {
        this.critHitChanceVal.value = val;
    }

    set critHitDamage(val) {
        this.critHitDamageVal.value = val;
    }

    set weaponDamage(val) {
        this.weaponDamageVal.value = val;
    }

    set weaponBaseAttacksPerSecond(val) {
        this.weaponBaseAttacksPerSecondVal.value = val;
    }

    set improvedAttackSpeedWeapon(val) {
        this.improvedAttackSpeedWeaponVal.value = val;
    }

    set improvedAttackSpeedGearAndParagon(val) {
        this.improvedAttackSpeedGearAndParagonVal.value = val;
    }

    set fetishDamageGidbin(val) {
        this.fetishDamageGidbinVal.value = val;
    }

    set fetishDamageSycophant(val) {
        this.fetishDamageSycophantVal.value = val;
    }

    set fetishArmyDartDamage(val) {
        this.fetishArmyDartDamageVal.value = val;
    }

    set fetishArmyMeleeDamage(val) {
        this.fetishArmyMeleeDamageVal.value = val;
    }

    set fetishTikiDamage(val) {
        this.fetishTikiDamageVal.value = val;
    }

    set fetishHeadHunterDamage(val) {
        this.fetishHeadHunterDamageVal.value = val;
    }

    set useMaskOfJeram(val) {
        this.useMaskOfJeramVal.value = val;
    }

    set mojPetIncrease(val) {
        this.mojPetIncreaseVal.value = val;
    }

    set coldDamagePercent(val) {
        this.coldDamagePercentVal.value = val;
    }

    set physicalDamagePercent(val) {
        this.physicalDamagePercentVal.value = val;
    }

    set fireDamagePercent(val) {
        this.fireDamagePercentVal.value = val;
    }

    set poisonDamagePercent(val) {
        this.poisonDamagePercentVal.value = val;
    }

    set fetishArmyDamagePercent(val) {
        this.fetishArmyDamagePercentVal.value = val;
    }

    set zombieDogsDamagePercent(val) {
        this.zombieDogsDamagePercentVal.value = val;
    }

    set gargantuanArmyDamagePercent(val) {
        this.gargantuanArmyDamagePercentVal.value = val;
    }

    set elitesDamagePercent(val) {
        this.elitesDamagePercentVal.value = val;
    }

    set poisonDartsDamagePercent(val) {
        this.poisonDartsDamagePercentVal.value = val;
    }

    set simplicityStrengthPercent(val) {
        this.simplicityStrengthPercentVal.value = val;
    }

    set enforcerGemPercent(val) {
        this.enforcerGemPercentVal.value = val;
    }

    set baneOfTheTrappedPercent(val) {
        this.baneOfTheTrappedPercentVal.value = val;
    }

    set baneOfThePowerfulPercent(val) {
        this.baneOfThePowerfulPercentVal.value = val;
    }

    set painEnhancerMobs(val) {
        this.painEnhancerMobsVal.value = val;
    }

    set gogokOfSwiftnessStacks(val) {
        this.gogokOfSwiftnessStacksVal.value = val;
    }

    set zeisStoneOfVengeancePercent(val) {
        this.zeisStoneOfVengeancePercentVal.value = val;
    }

    set useGemOfEffaciousToxin(val) {
        this.useGemOfEffaciousToxinVal.value = val;
    }

    set useTallMansFinger(val) {
        this.useTallMansFingerVal.value = val;
    }

    set numberOfDogsForTMF(val) {
        this.numberOfDogsForTMFVal.value = val;
    }

    set useTaskerAndTheo(val) {
        this.useTaskerAndTheoVal.value = val;
    }

    set percentIncreasePetAttackSpeed(val) {
        this.percentIncreasePetAttackSpeedVal.value = val;
    }

    set useHarringtonWaistguard(val) {
        this.useHarringtonWaistguardVal.value = val;
    }

    set percentHarringtonDamageBuff(val) {
        this.percentHarringtonDamageBuffVal.value = val;
    }

    set useOculus(val) {
        this.useOculusVal.value = val;
    }

    set percentOculusDamageBuff(val) {
        this.percentOculusDamageBuffVal.value = val;
    }

    set focusAndRestraint(val) {
        this.focusAndRestraintVal.value = val;
    }

    set useZuniSixPiece(val) {
        this.useZuniSixPieceVal.value = val;
    }

    set fetishArmyType(val) {
        this.fetishArmyTypeVal.value = val;
    }

    set zombieDogsType(val) {
        this.zombieDogsTypeVal.value = val;
    }

    set gargantuanType(val) {
        this.gargantuanTypeVal.value = val;
    }

    set useBBVSlamDance(val) {
        this.useBBVSlamDanceVal.value = val;
    }

    set totalBBVStackCharacter(val) {
        this.totalBBVStackCharacterVal.value = val;
    }

    set totalBBVStackPets(val) {
        this.totalBBVStackPetsVal.value = val;
    }

    set multiplayerIncreasedAttackSpeedBuff(val) {
        this.multiplayerIncreasedAttackSpeedBuffVal.value = val;
    }

    set multiplayerDamageBuff(val) {
        this.multiplayerDamageBuffVal.value = val;
    }

    set multiplayerAttacksPerSecondBuff(val) {
        this.multiplayerAttacksPerSecondBuffVal.value = val;
    }

    set multiplayerPetAttacksPerSecondBuff(val) {
        this.multiplayerPetAttacksPerSecondBuffVal.value = val;
    }

    set usePierceTheVeilPassive(val) {
        this.usePierceTheVeilPassiveVal.value = val;
    }

    set sacrificeProvokeThePackDogs(val) {
        this.sacrificeProvokeThePackDogsVal.value = val;
    }

    set useMcParanoia(val) {
        this.useMcParanoiaVal.value = val;
    }

    set useHaunted(val) {
        this.useHauntedVal.value = val;
    }

    set useChilledToTheBone(val) {
        this.useChilledToTheBoneVal.value = val;
    }

    set useHexJinx(val) {
        this.useHexJinxVal.value = val;
    }

    set usePiranhas(val) {
        this.usePiranhasVal.value = val;
    }

    set percentStrongarmKnockback(val) {
        this.percentStrongarmKnockbackVal.value = val;
    }

    set useMidnightFeastPassive(val) {
        this.useMidnightFeastPassiveVal.value = val;
    }

    set numberOfStacksSoulHarvest(val) {
        this.numberOfStacksSoilHarvestVal.value = val;
    }

    set numberOfStacksGFIntelBuff(val) {
        this.numberOfStacksGFIntelBuffVal.value = val;
    }

    static get focusAndRestraintOptions() {
        return [
            {label: 'None', value: 'none'},
            {label: 'Spender', value: 'spender'},
            {label: 'Generator', value: 'generator'},
            {label: 'Both', value: 'both'}
        ];
    }

    static get fetishArmyRuneOptions() {
        return [
            {label: 'Head Hunter', value: 'headhunter'},
            {label: 'Ambush', value: 'ambush'},
            {label: 'Dagger', value: 'dagger'},
            {label: 'Tiki', value: 'tiki'}
        ];
    }

    static get zombieDogsRuneOptions() {
        return [
            {label: 'Chilled', value: 'chilled'},
            {label: 'Burning', value: 'burning'},
            {label: 'Rabid', value: 'rabid'},
            {label: 'Physical', value: 'physical'}
        ];
    }

    static get gargantuanRuneOptions() {
        return [
            {label: 'Big Stinker', value: 'bigstinker'},
            {label: 'Bruiser', value: 'bruiser'},
            {label: 'Wrathful Protector', value: 'wrathfulprotector'},
            {label: 'Restless Giant', value: 'restlessgiant'},
            {label: 'Humongoid', value: 'humongoid'}
        ];
    }
}