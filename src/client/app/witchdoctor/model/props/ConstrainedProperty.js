import Property from './Property';

export default class ConstrainedProperty extends Property {
    constructor(val, options) {
        super(val);

        this.options = options;
    }

    set value(val) {
        let found = this.options.map((option) =>
            {return option.value === val});

        if(found) {
            super.value = val;
        }
    }

    get value() {
        return super.value;
    }
}