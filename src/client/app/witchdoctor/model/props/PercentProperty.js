import Property from './Property';

export default class PercentProperty extends Property {
    constructor(val) {
        super(val);
    }

    set value(val) {
        if(val > 100) {
            super.value = 100;
        } else {
            super.value = val;
        }
    }

    get value() {
        return super.value;
    }
}