import Property from './Property';

export default class TrueFalseProperty extends Property {
    constructor(prop) {
        super(prop);
    }

    set value(val) {
        if(val) {
            super.value = true;
        } else {
            super.value = false;
        }
    }

    get value() {
        return super.value;
    }
}