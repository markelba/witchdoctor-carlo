import Property from './Property';

export default class CappedProperty extends Property {
    constructor(val, cap) {
        super(val);
        this.cap = cap;
    }

    set value(val) {
        if(val > cap) {
            super.value = cap;
        } else {
            super.value = val;
        }
    }

    get value() {
        return super.value;
    }

    get maxValue() { return this.cap; }
    set maxValue(val) { this.cap = val; }
}