export default class Property {
    constructor(val) {
        this.fixedValue = false;
        this.propValue = val;
    }

    get isFixedValue() { return this.fixed; }
    set isFixedValue(val) { this.fixed = val; }
    fix() { this.fixed = true; }

    get value() { return this.propValue }
    set value(val) {
        if(val < 0) {
            this.propValue = 0;
        } else {
            this.propValue = val;
        }
    }
}