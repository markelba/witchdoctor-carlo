import React from 'react';
import DataRowNumeric from './components/DataRowNumeric';
import DataRowBoolean from './components/DataRowBoolean';
import DataRowSelect from './components/DataRowSelect';
import PlayerStatModel from './model/PlayerStatModel';

export default class WitchDoctorBasicInfo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            playerStatModel : new PlayerStatModel()
        }
    }

    render() {
        return (
            <div className="container">
                <h2>Stats</h2>
                <div className="row">
                    <div className="col-lg-6">
                        <h3>Player Stats</h3>
                        <DataRowNumeric title="Intelligence" inputId="intelligence" value={this.state.playerStatModel.intelligence}
                                        extraPropTitle="Buffed" extraPropValue="0" rowClassName="row"/>
                        <DataRowNumeric title="Sheet Crit Hit Chance" inputId="sheetCritHitChance" value={this.state.playerStatModel.critHitChance}
                                        rowClassName="row"/>
                        <DataRowNumeric title="Crit Hit Damage %" inputId="sheetCritHitDamage" value={this.state.playerStatModel.critHitDamage}
                                        rowClassName="row"/>
                        <DataRowNumeric title="Weapon Damage (min)" inputId="weaponDamage" value={this.state.playerStatModel.weaponDamage}
                                        rowClassName="row"/>
                        <DataRowNumeric title="Weapon base APS" inputId="weaponBaseAttacksPerSecond" value={this.state.playerStatModel.weaponBaseAttacksPerSecond}
                                        rowClassName="row" extraPropTitle="Calculated Base APS" extraPropValue="0"/>
                        <DataRowNumeric title="IAS on Weapon (%)" inputId="improvedAttackSpeedWeapon" value={this.state.playerStatModel.improvedAttackSpeedWeapon}
                                        rowClassName="row" extraPropTitle="Calculated Total APS" extraPropValue="0"/>
                        <DataRowNumeric title="IAS on Gear and Paragon Points (%)" inputId="improvedAttackSpeedGearAndParagon"
                                        value={this.state.playerStatModel.improvedAttackSpeedGearAndParagon} rowClassName="row"
                                        extraPropTitle="Buffed APS (all factors included)" extraPropValue="0"/>
                    </div>

                    <div className="col-lg-6">
                        <h3>Fetish Army Damage</h3>
                        <DataRowNumeric title="Gidbinn (no Element, Sheet Damage/APS)" inputId="fetishDamageGidbinn" value={this.state.playerStatModel.fetishDamageGidbin} extraPropTitle="Critical"
                            extraPropValue="0" rowClassName="row"/>
                        <DataRowNumeric title="Sycophant (180%, aligned to highest gear element)" inputId="fetishDamageSycophant" value={this.state.playerStatModel.fetishDamageSycophant}
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Army or Sycophant - Dart (130%, Poison, Carnevil)" inputId="fetishArmyDartDamage"
                                        value={this.state.playerStatModel.fetishArmyDartDamage} rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Army - Melee (180%, Element depends on rune)" inputId="fetishArmyMeleeDamage"
                                        value={this.state.playerStatModel.fetishArmyMeleeDamage} rowClassName="row" extraPropTitle="Critical" extraPropValue="0"/>
                        <DataRowNumeric title="Army - Tiki (85%, Fire)" inputId="fetishTikiDamage" value={this.state.playerStatModel.fetishTikiDamage}
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Army - Head Hunter (130%, poison)" inputId="fetishHeadHunterDamage" value={this.state.playerStatModel.fetishHeadHunterDamage}
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                    </div>
                </div>

                <h2>Gear</h2>
                <hr />
                <div className="row">
                    <div className="col-lg-6">
                        <DataRowBoolean title="Use Mask of Jeram?" inputId="useMaskOfJeram" checked={this.state.playerStatModel.useMaskOfJeram}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="% pet damage" inputId="mojPetIncrease" value={this.state.playerStatModel.mojPetIncrease}
                                        rowClassName="row"
                                        extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="% cold" inputId="coldDamagePercent" value={this.state.playerStatModel.coldDamagePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"
                                        extraExtraPropTitle="Jeram + %Element + Enforcer" extraExtraPropValue="1.0"/>
                        <DataRowNumeric title="% physical" inputId="physicalDamagePercent"
                                        value={this.state.playerStatModel.physicalDamagePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"
                                        extraExtraPropTitle="Jeram + %Element + Enforcer" extraExtraPropValue="1.0"/>
                        <DataRowNumeric title="% fire" inputId="fireDamagePercent" value={this.state.playerStatModel.fireDamagePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"
                                        extraExtraPropTitle="Jeram + %Element + Enforcer" extraExtraPropValue="1.0"/>
                        <DataRowNumeric title="% poison" inputId="poisonDamagePercent" value={this.state.playerStatModel.poisonDamagePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"
                                        extraExtraPropTitle="Jeram + %Element + Enforcer" extraExtraPropValue="1.0"/>
                        <DataRowNumeric title="% Fetish Army" inputId="fetishArmyDamagePercent"
                                        value={this.state.playerStatModel.fetishArmyDamagePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                        <DataRowNumeric title="% Summon Zombie Dogs" inputId="zombieDogsDamagePercent"
                                        value={this.state.playerStatModel.zombieDogsDamagePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                        <DataRowNumeric title="% Gargantuan" inputId="gargantuanArmyDamagePercent"
                                        value={this.state.playerStatModel.gargantuanArmyDamagePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                        <DataRowNumeric title="% Elites" inputId="elitesDamagePercent" value={this.state.playerStatModel.elitesDamagePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                        <DataRowNumeric title="% Poison Darts" inputId="poisonDartsDamagePercent"
                                        value={this.state.playerStatModel.elitesDamagePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                    </div>

                    <div className="col-lg-6">
                        <h3>Fetish Army Damage vs Elites</h3>
                        <DataRowNumeric title="Gidbinn (no Element, Sheet Damage/APS)" inputId="fetishDamageGidbinnElite" value="0" extraPropTitle="Critical"
                                        extraPropValue="0" rowClassName="row"/>
                        <DataRowNumeric title="Sycophant (180%, aligned to highest gear element)" inputId="fetishDamageSycophantElite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Army or Sycophant - Dart (130%, Poison, Carnevil)" inputId="fetishArmyDartDamageElite"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Army - Melee (180%, Element depends on rune)" inputId="fetishArmyMeleeDamageElite"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0"/>
                        <DataRowNumeric title="Army - Tiki (85%, Fire)" inputId="fetishTikiDamageElite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Army - Head Hunter (130%, poison)" inputId="fetishHeadHunterDamageElite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />

                        <h3>Zombie Dog Damage</h3>
                        <DataRowNumeric title="Single Dog (Type 1 attack, 30%)" inputId="zombieDogDamageType1" value="0" extraPropTitle="Critical"
                                        extraPropValue="0" rowClassName="row"/>
                        <DataRowNumeric title="Single Dog (Type 2 attack, 30%)" inputId="zombieDogDamageType2" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Single Burning Dog Aura (Fire, 2 ticks at 5% each)" inputId="zombieDogDamageBurning"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="TMF Dog (Type 1 attack, 30%, no F&R)" inputId="zombieDogTMFDamageType1"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0"/>
                        <DataRowNumeric title="TMF Dog (Type 2 attack, 30%, no F&R)" inputId="zombieDogTMFDamageType2" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="TMF Burning Dog Aura (Fire, 2 ticks at 5% each, no F&R)" inputId="zombieDogTMFDamageBurning" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="TMF Rabid Dog (30%, 30% decay, Poison, no F&R)" inputId="zombieDogTMFDamageRabid" value="0"
                                        rowClassName="row" />
                    </div>
                </div>

                <h3>Gems</h3>
                <hr />
                <div className="row">
                    <div className="col-lg-6">
                        <DataRowNumeric title="Simplicity's Strength (%)" inputId="simplicityStrengthPercent"
                                        value={this.state.playerStatModel.simplicityStrengthPercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Enforcer Gem (%)" inputId="enforcerGemPercent" value={this.state.playerStatModel.enforcerGemPercent}
                                        rowClassName="row"
                                        extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Bane of the Trapped Gem (%)" inputId="baneOfTheTrappedPercent"
                                        value={this.state.playerStatModel.baneOfTheTrappedPercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Bane of the Powerful (%)" inputId="baneOfThePowerfulPercent"
                                        value={this.state.playerStatModel.baneOfThePowerfulPercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Pain Enhancer (# of mobs bleeding)" inputId="painEnhancerMobs"
                                        value={this.state.playerStatModel.painEnhancerMobs}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Gogok of Swiftness (# stacks)" inputId="gogokOfSwiftnessStacks"
                                        value={this.state.playerStatModel.gogokOfSwiftnessStacks}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Zei's Stone of Vengeance (%)" inputId="zeisStoneOfVengeancePercent"
                                        value={this.state.playerStatModel.zeisStoneOfVengeancePercent}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                        <DataRowBoolean title="Use Gem of Effacious Toxin?" inputId="useGemOfEffaciousToxin"
                                        value={this.state.playerStatModel.useGemOfEffaciousToxin}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                    </div>

                    <div className="col-lg-6">
                        <h3>Zombie Dog Damage vs Elites</h3>
                        <DataRowNumeric title="Single Dog (Type 1 attack, 30%)" inputId="zombieDogDamageType1Elite" value="0" extraPropTitle="Critical"
                                        extraPropValue="0" rowClassName="row"/>
                        <DataRowNumeric title="Single Dog (Type 2 attack, 30%)" inputId="zombieDogDamageType2Elite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Single Burning Dog Aura (Fire, 2 ticks at 5% each)" inputId="zombieDogDamageBurningElite"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="TMF Dog (Type 1 attack, 30%, no F&R)" inputId="zombieDogTMFDamageType1Elite"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0"/>
                        <DataRowNumeric title="TMF Dog (Type 2 attack, 30%, no F&R)" inputId="zombieDogTMFDamageType2Elite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="TMF Burning Dog Aura (Fire, 2 ticks at 5% each, no F&R)" inputId="zombieDogTMFDamageBurningElite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="TMF Rabid Dog (30%, 30% decay, Poison, no F&R)" inputId="zombieDogTMFDamageRabidElite" value="0"
                                        rowClassName="row" />
                    </div>
                </div>

                <h2>Legendary Abilities</h2>
                <hr />
                <div className="row">
                    <div className="col-lg-6">
                        <DataRowBoolean title="Use Tall Man's Finger?" inputId="useTallMansFinger"
                                        checked={this.state.playerStatModel.useTallMansFinger}
                                        rowClassName="row"/>
                        <DataRowNumeric title="Number of Dogs for TMF" inputId="numberOfDogsForTMF" value={this.state.playerStatModel.numberOfDogsForTMF}
                                        rowClassName="row"
                                        extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowBoolean title="Use Tasker and Theo?" inputId="useTaskerAndTheo"
                                        checked={this.state.playerStatModel.useTaskerAndTheo}
                                        rowClassName="row"/>
                        <DataRowNumeric title="% increase Pet Attack Speed" inputId="percentIncreasePetAttackSpeed"
                                        value={this.state.playerStatModel.percentIncreasePetAttackSpeed}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowBoolean title="Use Harrington Waistguard (clickable activated)"
                                        inputId="useHarringtonWaistguard" checked={this.state.playerStatModel.useHarringtonWaistguard}
                                        rowClassName="row"/>
                        <DataRowNumeric title="% Harrington Damage Buff" inputId="percentHarringtonDamageBuff"
                                        value={this.state.playerStatModel.percentHarringtonDamageBuff}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowBoolean title="Use Oculus (WD in AOE)?" inputId="useOculus" checked={this.state.playerStatModel.useOculus}
                                        rowClassName="row"/>
                        <DataRowNumeric title="% Oculus Damage Buff" inputId="percentOculusDamageBuff" value={this.state.playerStatModel.percentOculusDamageBuff}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                        <DataRowSelect title="Use Focus and Restraint?" inputId="focusAndRestraint"
                                       options={PlayerStatModel.focusAndRestraintOptions} value={this.state.playerStatModel.focusAndRestraint}
                                       rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                        <DataRowBoolean title="Use Zuni's 6-piece (target debuffed with spender skill)?"
                                        inputId="useZuniSixPiece" value={this.state.playerStatModel.useZuniSixPiece}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                    </div>

                    <div className="col-lg-6">
                        <h3>Gargantuan Damage</h3>
                        <DataRowNumeric title="Humongoid (130%, Cold, Cleave)" inputId="gargantuanDamageHumongoid" value="0" extraPropTitle="Critical"
                                        extraPropValue="0" rowClassName="row"/>
                        <DataRowNumeric title="Restless Giant (100%, Physical)" inputId="gargantuanDamageRestlessGiant" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Restless Giant (300%, Physical, Rune activated)" inputId="gargantuanDamageRestlessGiantActive"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Wrathful (575% Fire, Cleave, summoned in all IAS buffs)" inputId="gargantuanDamageWrathfulWithIAS"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0"/>
                        <DataRowNumeric title="Wrathful (575% Fire, Cleave, summoned without IAS buffs)" inputId="gargantuanDamageWrathful" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Big Stinker (100%, Poison)" inputId="gargantuanDamageBigStinker" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Big Stinker Aura (45%, Poison)" inputId="zombieDogTMFDamageRabid" value="0"
                                        rowClassName="row" />
                        <DataRowNumeric title="Bruiser (200%, Fire)" inputId="gargantuanDamageBruiser" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                    </div>
                </div>

                <h2>Pet Runes</h2>
                <hr />
                <div className="row">
                    <div className="col-lg-8">
                        <DataRowSelect title="Select Fetish Army Rune" inputId="fetishArmyType"
                                       options={PlayerStatModel.fetishArmyRuneOptions} value={this.state.playerStatModel.fetishArmyType}
                                       rowClassName="row" extraPropTitle="Element" extraPropValue="None"/>
                        <DataRowSelect title="Select Zombie Dogs Rune" inputId="zombieDogsType"
                                       options={PlayerStatModel.zombieDogsRuneOptions} value={this.state.playerStatModel.zombieDogsType}
                                       rowClassName="row" extraPropTitle="Element" extraPropValue="None"/>
                        <DataRowSelect title="Select Gargantuan Rune" inputId="gargantuanType"
                                       options={PlayerStatModel.gargantuanRuneOptions} value={this.state.playerStatModel.gargantuanType}
                                       rowClassName="row" extraPropTitle="Element" extraPropValue="None"/>
                    </div>
                </div>

                <h2>Multiplayer and Group Abilities</h2>
                <hr />
                <div className="row">
                    <div className="col-lg-6">
                        <DataRowBoolean title="Use BBV: Slam Dance (WD in AOE)?" inputId="useBBVSlamDance"
                                        checked={this.state.playerStatModel.useBBVSlamDance}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Total BBV Stack (+20% AS per different rune, WD in AOE)"
                                        inputId="totalBBVStackCharacter" value={this.state.playerStatModel.totalBBVStackCharacter}
                                        rowClassName="row"
                                        extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Total BBV Stack (+20% AS per different rune, Pets in AOE)"
                                        inputId="totalBBVStackPets" value={this.state.playerStatModel.totalBBVStackPets}
                                        rowClassName="row"
                                        extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="% Other multiplayer %IAS Buff (WD affected)"
                                        inputId="multiplayerIncreasedAttackSpeedBuff" value={this.state.playerStatModel.multiplayerIncreasedAttackSpeedBuff}
                                        rowClassName="row"
                                        extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="% Other multiplayer Damage Buff & Monster Debuff"
                                        inputId="multiplayerDamageBuff" value={this.state.playerStatModel.multiplayerDamageBuff}
                                        rowClassName="row"
                                        extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="% Other multiplayer pet APS buff"
                                        inputId="multiplayerPetAttacksPerSecondBuff" value={this.state.playerStatModel.multiplayerPetAttacksPerSecondBuff}
                                        rowClassName="row"
                                        extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowBoolean title="Use PTV passive?" inputId="usePierceTheVeilPassive"
                                        checked={this.state.playerStatModel.usePierceTheVeilPassive}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Number of Dogs Sacrificed for Provoke the Pack"
                                        inputId="sacrificeProvokeThePackDogs" value={this.state.playerStatModel.sacrificeProvokeThePackDogs}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowBoolean title="Use MC: Paranoia?" inputId="useMcParanoia" checked={this.state.playerStatModel.useMcParanoia}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowBoolean title="Use Poison Spirit (target Haunted)?" inputId="useHaunted"
                                        checked={this.state.playerStatModel.useHaunted}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowBoolean title="Use Chilled to the Bone (target Chilled)?" inputId="useChilledToTheBone"
                                        checked={this.state.playerStatModel.useChilledToTheBone}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowBoolean title="Use Hex: Jinx (target Hexed)?" inputId="useHexJinx"
                                        checked={this.state.playerStatModel.useHexJinx}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowBoolean title="Use Piranhas? (mobs affected)?" inputId="usePiranhas"
                                        checked={this.state.playerStatModel.usePiranhas}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="% Knockback From Strongarm Bracer" inputId="percentStrongarmKnockback"
                                        value={this.state.playerStatModel.percentStrongarmKnockback}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>

                        <div className="row">
                            <div className="col-sm-6"><strong>Total multiplayer buff and monster debuff</strong></div>
                            <div className="col-sm-6"><strong>0</strong></div>
                        </div>

                        <DataRowBoolean title="Use Midnight Feast Passive?" inputId="useMidnightFeastPassive"
                                        checked={this.state.playerStatModel.useMidnightFeastPassive}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="1.0"/>
                        <DataRowNumeric title="Number of Stacks Soul Harvested" inputId="numberOfStacksSoulHarvest"
                                        value={this.state.playerStatModel.numberOfStacksSoulHarvest}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>
                        <DataRowNumeric title="Number of stacks for GF intel buff" inputId="numberOfStacksGFIntelBuff"
                                        value={this.state.playerStatModel.numberOfStacksGFIntelBuff}
                                        rowClassName="row" extraPropTitle="Multiplier" extraPropValue="0"/>

                    </div>

                    <div className="col-lg-6">
                        <h3>Gargantuan Damage vs Elites</h3>
                        <DataRowNumeric title="Humongoid (130%, Cold, Cleave)" inputId="gargantuanDamageHumongoidElite" value="0" extraPropTitle="Critical"
                                        extraPropValue="0" rowClassName="row"/>
                        <DataRowNumeric title="Restless Giant (100%, Physical)" inputId="gargantuanDamageRestlessGiantElite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Restless Giant (300%, Physical, Rune activated)" inputId="gargantuanDamageRestlessGiantActiveElite"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Wrathful (575% Fire, Cleave, summoned in all IAS buffs)" inputId="gargantuanDamageWrathfulWithIASElite"
                                        value="0" rowClassName="row" extraPropTitle="Critical" extraPropValue="0"/>
                        <DataRowNumeric title="Wrathful (575% Fire, Cleave, summoned without IAS buffs)" inputId="gargantuanDamageWrathfulElite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Big Stinker (100%, Poison)" inputId="gargantuanDamageBigStinkerElite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                        <DataRowNumeric title="Big Stinker Aura (45%, Poison)" inputId="zombieDogTMFDamageRabidElite" value="0"
                                        rowClassName="row" />
                        <DataRowNumeric title="Bruiser (200%, Fire)" inputId="gargantuanDamageBruiserElite" value="0"
                                        rowClassName="row" extraPropTitle="Critical" extraPropValue="0" />
                    </div>
                </div>
            </div>
        )
    }
}