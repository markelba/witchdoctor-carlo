import React from 'react';
import {render} from 'react-dom';
import WitchDoctorBasicInfo from './witchdoctor/basic-info';
require('bootstrap/dist/css/bootstrap.css');


render((<div>
        <h1>Monte Carlo Witchdoctor Simulation</h1>
        <WitchDoctorBasicInfo/>
    </div>)
    , document.getElementById("app"));